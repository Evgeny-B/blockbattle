﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public Figure figure;
    public CubeBaseFigure baseFigure;
    public GameLogic gameLogic;
    public LevelGenerator levelGenerator;
    public ColorScheme colorScheme;
    public CameraControl cameraControl;

    public enum ETutorialState {control, rule, goal, end};
    bool beforeControl = true;
    bool beforeRule = true;
    bool beforeGoal = true;

    bool isDemoCameraAnim = false;
    float timeToRotate = 3.0f;

    public ETutorialState tutorialState;

    public GameObject ruleSection;

    public float timeToGoalSection = 5.0f;
    public GameObject goalSection;

    public Dictionary<EFigureControlInterface,KeyCode> controlBinding;

    float timerAnim;

    Transform targetCube;
    GameObject point;

    void Awake ()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        colorScheme = gameLogic.transform.GetComponent<ColorScheme> ();

        tutorialState = ETutorialState.control;

        controlBinding = StaticLogic.controlBinding;

        ruleSection.SetActive (false);
        goalSection.SetActive (false);

        if (figure)
        {
            figure.isPauseFalling = true;
            figure.BornLevel = 5;
        }

        if (!cameraControl)
            isDemoCameraAnim = false;
        else
        {
            isDemoCameraAnim = gameLogic.isDemoCameraAnim;
            timeToRotate = gameLogic.timeToRotate;
            cameraControl.setCameraRotation (ERotating.right);
        }

        timerAnim = 0;

        levelGenerator.Init (ELevelNames.basic_rule);

        LevelSettingsStatic.calibrate = 3;
        baseFigure.Init ();
        HideBaseFigure ();

        foreach (Transform child in baseFigure.transform) 
            if ( ( child.GetComponent<CubeAttributes> ().level_z == 0 )
                && ( child.GetComponent<CubeAttributes> ().level_y == 0 )
                && ( child.GetComponent<CubeAttributes> ().level_x == 0 ) 
                && ( child.GetComponent<CubeAttributes> ().type == CubeType.regular ) )
                targetCube = child;

        //point = GameObject.Find("TargetPoint");
        point = GameObject.Find("HighlightAnim");
        point.SetActive (false);
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKeyDown ( KeyCode.Return ) )
            tutorialState = ETutorialState.end;

        if ( baseFigure.destroyedGoldCubes > 0 )
            tutorialState = ETutorialState.end;
            
        switch (tutorialState)
        {
            case ETutorialState.control:

                if (beforeControl)
                {
                    figure.SetColors ( colorScheme.firstColor, colorScheme.thirdColor );
                    beforeControl = false;
                }

                if (
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_down ] ) )  ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_up ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_right ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_left ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_counterclockwise ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_clockwise ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_right ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_up ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_down ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_left ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.fall ] ) )
                )
                {
                    tutorialState = ETutorialState.rule;
                    ruleSection.SetActive (true);
                }

                break;            
            case ETutorialState.rule:

                //timer = timer + Time.deltaTime;
                //if ( timer >= timeToGoalSection )
                //{
                //    tutorialState = ETutorialState.goal;
                //    goalSection.SetActive (true);
                //}

                ShowBaseFigure ();

                //TODO: Fix this mess
                if (isDemoCameraAnim)
                {
                    timerAnim = timerAnim + Time.deltaTime;
                    if ( timerAnim >= timeToRotate )
                    {
                        if (beforeRule)
                        {
                            if (isDemoCameraAnim)
                                cameraControl.rotating = ERotating.left;

                            beforeRule = false;
                        }

                        if ( (!beforeRule) && (cameraControl.rotating == ERotating.no) )
                            point.SetActive (true);
                    }
                }
                else
                if (beforeRule)
                {
                    point.SetActive (true);

                    beforeRule = false;
                }

                if ( targetCube == null )
                        tutorialState = ETutorialState.goal; 

                break;
            case ETutorialState.goal:

                if (beforeGoal)
                {
                    figure.SetColors ( colorScheme.secondColor, colorScheme.thirdColor );

                    foreach (GameObject gobj in GameObject.FindGameObjectsWithTag("Tutorial Rule Section"))
                        gobj.SetActive (false);

                    ruleSection.SetActive (false);

                    beforeGoal = false;
                }

                goalSection.SetActive (true);

                break;
            case ETutorialState.end:

                //Debug.Log ( "Tutorial ending phase" );
                gameLogic.isOneColor = false;
                LevelSettingsStatic.firstGold = false;

                LevelSettingsStatic.calibrate = 1;
                
                levelGenerator.Init (ELevelNames.candle);

                baseFigure.Reset ();

                cameraControl.setCameraRotation (ERotating.right);
                

                figure.isPauseFalling = false;
                figure.BornLevel = 8;

                figure.Reset ();

                this.gameObject.SetActive (false);

                gameLogic.isTutorialOn = false;

                break;
        }
    }

    void HideBaseFigure ()
    {
        foreach (Transform child in baseFigure.transform) 
            //child.GetComponent<MeshRenderer>().enabled = false;
            child.gameObject.SetActive (false);
    }

    void HideHalfBaseFigure ()
    {
        foreach (Transform child in baseFigure.transform) 
            if ( child.GetComponent<CubeAttributes> ().level_z > 0 )
                child.gameObject.SetActive (false);
    }

    void ShowBaseFigure ()
    {
        foreach (Transform child in baseFigure.transform) 
            //child.GetComponent<MeshRenderer>().enabled = true;
            child.gameObject.SetActive (true);
    }
}
