﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoFigure : Figure
{
    // Start is called before the first frame update
    void Start()
    {
        controlBinding = StaticLogic.InitControlBinding ();

        rotating = ERotating.no;
        movement = EMoveFigure.no;

        transform.GetChild (0).GetComponent<CubeEdges>().setColor(CubeColors.red);
        transform.GetChild (1).GetComponent<CubeEdges>().setColor(CubeColors.yellow);
    }

    // Update is called once per frame
    void Update()
    {
        StaticLogic.RotateObject (ref rotating, ref angle, rotationSpeed, transform.position, transform);

        MoveFigure ( 5.0f );

        if (rotating == ERotating.no)
        {
            angle = 0;
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_down ] ) )
                rotating = ERotating.down;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_up ] ) )
                rotating = ERotating.up;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_right ] ) )
                rotating = ERotating.right;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_left ] ) )
                rotating = ERotating.left;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_counterclockwise ] ) )
                rotating = ERotating.counterclockwise;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_clockwise ] ) )
                rotating = ERotating.clockwise;
        }

        if (movement == EMoveFigure.no)
        {
            move = 0;
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_right ] ) )
            {
                movement = EMoveFigure.right;
            }
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_up ] ) )
            {
                movement = EMoveFigure.up;
            }
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_down ] ) )
            {
                movement = EMoveFigure.down;
            }
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_left ] ) )
            {
                movement = EMoveFigure.left;
            }
        }
    }
}
