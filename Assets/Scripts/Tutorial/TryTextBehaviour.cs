﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TryTextBehaviour : MonoBehaviour
{
    public float initialDelay = 5.0f;
    public float blinkingTime = 1.5f;

    float timer;
    bool isOn;
    bool stopped;
    public Dictionary<EFigureControlInterface,KeyCode> controlBinding;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<TextMeshProUGUI>().enabled = false;
        timer = 0;
        isOn = false;
        stopped = false;

        controlBinding = StaticLogic.controlBinding;
    }

    // Update is called once per frame
    void Update()
    {

        if (!stopped)
        {
            timer = timer + Time.deltaTime;

            if ( ( !isOn ) && ( timer >= initialDelay ) )
            {
                isOn = true;
                timer = blinkingTime;
            }

            if (isOn)
            {
                if (timer >= blinkingTime)  
                {
                       GetComponent<TextMeshProUGUI>().enabled = true;
                }
                if (timer >= 2.0f * blinkingTime)
                {
                       GetComponent<TextMeshProUGUI>().enabled = false;
                       timer = 0;
                }
            }

            if ( 
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_down ] ) )  ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_up ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_right ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_left ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_counterclockwise ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_clockwise ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_right ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_up ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_down ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_left ] ) ) ||
                ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.fall ] ) )
                )
            {
                stopped = true;
                GetComponent<TextMeshProUGUI>().enabled = false;
            }
        }
    }
}
