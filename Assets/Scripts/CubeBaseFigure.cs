using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/*
 * Thic class implement base figure of cubes. Player should destroy it
 * 
 * Methods:
 * AttachFigureCube (EDirection dir, CubeColors col, Vector3 hitpos) - Attach one cube to hitted cube center _hitpos_ in 
 *      direction _dir_ and color _col_. Add this cube to chain of attached cubes.
 * bool DestroyColorChainsOfCubes () - Look for chains of cubes same color size of >= 3. Destroy it. 
 *      Hanging peaces will fall toward to base figure. Return true if there were hanging peaces and 
 *      this function need to be called again (unity feature of raycast and moving colliders)
 *  
 */
public class CubeBaseFigure : MonoBehaviour
{
    public Transform regularCube;
    public Transform centerCube;
    public Transform crackedCube;

    public TextMeshProUGUI goldCubesLeftLabel;

    public SoundEffects soundEffects;

    List<CubePointPosition> grid;

    public float dxInit; // x offset position (relative to camera) for figure initial position
    public float dyInit; // y offset position (relative to camera) for figure initial position

    float destroyTimer;
    public float destroyTime = 1; // Seconds from fell to born new figure

    public int destroyedCubesPlayer = 0; // Number of destroyed cubes by Player
    public int totalRegularCubes = 0;
    public int totalGoldCubes = 0;
    public int destroyedRegularCubes = 0;
    public int destroyedGoldCubes = 0;

    public Vector3Int pit = 7 * Vector3Int.one;
    public float defaultBaseFigureDiameter = 7;
    public float defaultCenterDiameter = 1;

    public LevelGenerator levelGenerator; // link to generator class
    CubeConstructor regularConstructor; // link to generator functions
    CubeConstructor goldConstructor; // link to generator function

    float startX;
    float startY;
    float startZ;

    List<CubeAttributes> goldCubes; // array of gold cubes

    List<CubeAttributes> attachedCubes; // array of new base figure's cubes which have been attached by this figure
    bool isAttachedCubesChecked = false; // show wether attached cubes were checked for destruction

    // arrays of neighbour cubes same color at the place where figure have been attached. Chains can be split
    List<CubeAttributes> firstColorChain; 
    List<CubeAttributes> secondColorChain;
    List<CubeAttributes> thirdColorChain;

    public ColorScheme colorScheme; // link for color scheme

    List<CubeAttributes> crackedChain; // All cracked cubes which need to be destroy

    int cubesInGraphComponent; // number of cubes in current graph component
    int numberOfGraphComponenets; // current number of components in graph

    List<CubeAttributes> hangingFigure; // array of hanging cubes

    public void Init ()
    {
        destroyTimer = 0;

        goldCubes = new List<CubeAttributes> ();

        attachedCubes = new List<CubeAttributes> ();

        firstColorChain = new List<CubeAttributes> ();
        secondColorChain = new List<CubeAttributes> ();
        thirdColorChain = new List<CubeAttributes> ();

        crackedChain = new List<CubeAttributes> ();

        hangingFigure = new List<CubeAttributes> ();

        destroyedRegularCubes = destroyedGoldCubes = totalRegularCubes = totalGoldCubes = 0;

        regularConstructor = defaultRegularConstructor;
        goldConstructor = defaultGoldConstructor;

        if ( ( levelGenerator != null ) && (LevelSettingsStatic.levelName != ELevelNames.none) )
        {
            regularConstructor = levelGenerator.regularConstructor;
            goldConstructor = levelGenerator.goldConstructor;
            pit = levelGenerator.pit;
        }

        InitPit ();

        generateCubes ();

        if (goldCubesLeftLabel)
        {
            //Debug.Log ( "Total gold cubes created: " + totalGoldCubes );
            goldCubesLeftLabel.SetText ( totalGoldCubes.ToString() );
        }

        Debug.Log ( "CubeBaseFigure NothingFall: " + LevelSettingsStatic.nothingFall.ToString());
        Debug.Log ( "Total cubes created: " + transform.childCount );
    }

    public void Reset ()
    {
        destroyTimer = 0;
        destroyedRegularCubes = destroyedGoldCubes = totalRegularCubes = totalGoldCubes = 0;

        goldCubes.Clear ();

        attachedCubes.Clear ();
        hangingFigure.Clear ();

        firstColorChain.Clear ();
        secondColorChain.Clear ();
        thirdColorChain.Clear ();

        crackedChain.Clear ();

        foreach (Transform child in transform) 
            GameObject.Destroy(child.gameObject);

        regularConstructor = defaultRegularConstructor;
        goldConstructor = defaultGoldConstructor;

        if ( ( levelGenerator != null ) && (LevelSettingsStatic.levelName != ELevelNames.none) )
        {
            regularConstructor = levelGenerator.regularConstructor;
            goldConstructor = levelGenerator.goldConstructor;
            pit = levelGenerator.pit;
        }

        InitPit ();

        generateCubes ();

        if (goldCubesLeftLabel)
            goldCubesLeftLabel.SetText ( totalGoldCubes.ToString() );

        Debug.Log ( "Total cubes created: " + transform.childCount );
    }

    void InitPit ()
    {
        startX = transform.position.x - (float)( pit.x - 1 )*CubePointPosition.size/2.0f;
        startY = transform.position.y - (float)( pit.y - 1 )*CubePointPosition.size/2.0f;
        startZ = transform.position.z - (float)( pit.z - 1 )*CubePointPosition.size/2.0f;
        
        dxInit = CubePointPosition.size/2.0f * (float)( ( pit.x - 1 ) % 2);
        dyInit = CubePointPosition.size/2.0f * (float)( ( pit.y - 1 ) % 2);        
    }

    void generateCubes ()
    {
        Transform temp;
        Transform newCube;
        CubeColors c;
        CubeCoords coords;
        int val_r; // result of regular cube construction's functions
        int val_c; // result of gold cube construction's functions

        //Debug.Log ( "Pit size: " + pit.ToString () );
        for (int i = 0; i < pit.x; i++)
            for (int j = 0; j < pit.y; j++)
                for (int k = 0; k < pit.z; k++)
            {
                val_r = regularConstructor ( i, j, k );
                // for demo only
                // val_r = _tmp_defaultRegularConstructor2 ( i, j, k );
                //Debug.Log ( "val_r (" + i + ", " + j + ", " + k + "): " + val_r );
                val_c = goldConstructor ( i, j, k );
                //Debug.Log ( "val_c (" + i + ", " + j + ", " + k + "): " + val_c );

                if ( val_c > 0 )
                {
                    c = colorScheme.colors [ val_c - 1 ];
                    coords = new CubeCoords( (float)i - (float)(pit.x - 1)/2, (float)j - (float)(pit.y - 1)/2, (float)k - (float)(pit.z - 1)/2 );

                    newCube = centerCube;
                    temp = Instantiate(newCube, new Vector3(
                        startX + CubePointPosition.size * i, 
                        startY + CubePointPosition.size * j,
                        startZ + CubePointPosition.size * k), 
                        Quaternion.identity ) as Transform;
                                       
                    temp.GetComponent<CubeAttributes>().Init (c, coords, CubeType.gold, isBase_: true);
                    
                    temp.parent = this.gameObject.transform;

                    goldCubes.Add ( temp.GetComponent<CubeAttributes>() );

                    totalGoldCubes += 1;
                }
                else
                if ( val_r > 0 )
                {
                    newCube = regularCube;
                    c = colorScheme.colors [ val_r - 1 ];
                    coords = new CubeCoords( (float)i - (float)(pit.x - 1)/2, (float)j - (float)(pit.y - 1)/2, (float)k - (float)(pit.z - 1)/2 );
                    //Debug.Log ( "reg cube coords: " + coords.ToString () );

                    temp = Instantiate(newCube, new Vector3(
                        startX + CubePointPosition.size * i, 
                        startY + CubePointPosition.size * j,
                        startZ + CubePointPosition.size * k), 
                        Quaternion.identity ) as Transform;
                                       
                    temp.GetComponent<CubeAttributes>().Init (c, coords, CubeType.regular, LevelSettingsStatic.calibrate, true);

                    temp.parent = this.gameObject.transform;

                    totalRegularCubes += 1;
                }
            }        
    }

    int defaultRegularConstructor ( int i, int j, int k )
    {
        int val = 0;
        float rad = defaultBaseFigureDiameter / 2.0f;
        float distance = rad * rad - 
            ((float)i + 0.5f - (float)pit.x/2) * ((float)i + 0.5f - (float)pit.x/2) - 
            ((float)j + 0.5f - (float)pit.y/2) * ((float)j + 0.5f - (float)pit.y/2) - 
            ((float)k + 0.5f - (float)pit.z/2) * ((float)k + 0.5f - (float)pit.z/2);

        if ( distance >= 0 )
            val = UnityEngine.Random.Range (1, 4);
            
        return val;
    }

    int defaultGoldConstructor ( int i, int j, int k )
    {
        int val = 0;
        float rad = defaultCenterDiameter / 2.0f;
        float distance = rad * rad - 
            ((float)i + 0.5f - (float)pit.x/2) * ((float)i + 0.5f - (float)pit.x/2) - 
            ((float)j + 0.5f - (float)pit.y/2) * ((float)j + 0.5f - (float)pit.y/2) - 
            ((float)k + 0.5f - (float)pit.z/2) * ((float)k + 0.5f - (float)pit.z/2);

        //Debug.Log ( rad + " d " + distance);

        if ( distance >= 0 )
            val = UnityEngine.Random.Range (1, 4);

        return val;
    }

    int _tmp_defaultRegularConstructor ( int i, int j, int k )
    {
        int val = UnityEngine.Random.Range (0, 8);

        val = val > 3 ? 0 : val;

        return val;
    }

    // That version looks bad
    int _tmp_defaultRegularConstructor2 ( int i, int j, int k )
    {
        int val = 0;

        int limit = Mathf.Abs (k - 3) > 2 ? 0 : 3 - Mathf.Abs (k - 3);

        if ( ( Mathf.Abs ( 2 - i ) < limit ) && 
             ( Mathf.Abs ( 2 - j ) < limit ) )
            val = UnityEngine.Random.Range (1, 4);

        return val;
    }

    void InitGrid (int fieldSize)
    {
        grid = new List<CubePointPosition>();

        for (int i = 0; i < fieldSize; i++)
            for (int j = 0; j < fieldSize; j++)
                for (int k = 0; k < fieldSize; k++)
            {
                grid.Add (new CubePointPosition (i + j * fieldSize + k * fieldSize * fieldSize));
            }

        for (int i = 0; i < fieldSize; i++)
            for (int j = 0; j < fieldSize; j++)
                for (int k = 0; k < fieldSize; k++)
            {
                grid [i + j * fieldSize + k * fieldSize * fieldSize].xLeft_ = 
                    grid [(fieldSize + i - 1) % fieldSize + j * fieldSize + k * fieldSize * fieldSize];
                grid [i + j * fieldSize + k * fieldSize * fieldSize].xRight_ = 
                    grid [(i + 1) % fieldSize + j * fieldSize + k * fieldSize * fieldSize];
                grid [i + j * fieldSize + k * fieldSize * fieldSize].yLeft_ = 
                    grid [i + ((fieldSize + j - 1) % fieldSize) * fieldSize + k * fieldSize * fieldSize];
                grid [i + j * fieldSize + k * fieldSize * fieldSize].yRight_ = 
                    grid [i + ((j + 1) % fieldSize) * fieldSize + k * fieldSize * fieldSize];
                grid [i + j * fieldSize + k * fieldSize * fieldSize].zLeft_ = 
                    grid [i + j * fieldSize + ((fieldSize + i - 1) % fieldSize) * fieldSize * fieldSize];
                grid [i + j * fieldSize + k * fieldSize * fieldSize].zRight_ = 
                    grid [i + j * fieldSize + ((k + 1) % fieldSize) * fieldSize * fieldSize];
            }
    }

    public Transform AttachFigureCube (EDirection dir, CubeColors col, Transform hitCube)
    {
        Transform temp;
        Vector3 pos = Vector3.zero;
        Vector3 hitpos = hitCube.position;
        CubeCoords newCoords, hitCubeCoords;

        switch (dir)
        {
        case EDirection.right:
            pos = Vector3.right;
            break;
        case EDirection.left:
            pos = -Vector3.right;
            break;
        case EDirection.up:
            pos = Vector3.up;
            break;
        case EDirection.down:
            pos = -Vector3.up;
            break;
        case EDirection.forward:
            pos = Vector3.forward;
            break;
        case EDirection.backward:
            pos = -Vector3.forward;
            break;
        }

        hitCubeCoords = hitCube.GetComponent<CubeAttributes>().coords;
        newCoords = new CubeCoords(pos.x + hitCubeCoords.x, pos.y + hitCubeCoords.y, pos.z + hitCubeCoords.z);

        pos = hitpos + pos * CubePointPosition.size;

        temp = Instantiate(regularCube, pos, Quaternion.identity) as Transform;
        // Debug.Log ("new " + col.ToString() + " cube created");
        temp.parent = this.gameObject.transform;
        temp.GetComponent<CubeAttributes>().Init (col, newCoords, calibrate_: LevelSettingsStatic.calibrate);
        // Debug.Log ("New cube level " + temp.GetComponent<CubeAttributes>().level);
                    
        attachedCubes.Add (temp.GetComponent<CubeAttributes> ());
        
        return temp;
    }

    public bool DestroyColorChainsOfCubes ()
    {
        return DestroyColorChainsOfCubes (attachedCubes);
    }

    bool DestroyColorChainsOfCubes (List<CubeAttributes> attachFigure)
    {
        bool res = true;
        destroyTimer += Time.deltaTime;
        
        //Debug.Log ("DestroyColorChainsOfCubes attachFigure.Count: " + attachFigure.Count);
        // We find all cubes that touches fallen figure and sort them into color chains
        if (!isAttachedCubesChecked)
        {
            foreach (CubeAttributes cubeAttr in attachFigure) 
            {
                Debug.Log ("Checking Cube Attr " + cubeAttr.ToString() );

                FindSameColorNeighbours (cubeAttr);
            }

            HighlightChains ();

            //CalculateScore ();

            isAttachedCubesChecked = true;
        }

        if (destroyTimer >= destroyTime)
        {
            if (firstColorChain.Count > 2)
                DestroyColorChain (firstColorChain);
            if (secondColorChain.Count > 2)
                DestroyColorChain (secondColorChain);
            if (thirdColorChain.Count > 2)
                DestroyColorChain (thirdColorChain);

            destroyedCubesPlayer += crackedChain.Count;
            if (crackedChain.Count > 0)
                DestroyColorChain (crackedChain, true, true);

            if (LevelSettingsStatic.nothingFall)
            {
                res = FallSingleCubes ();
            }

            EmptyColorChains ();

            if (!LevelSettingsStatic.nothingFall)
            {
                CheckGraphContinuety ();

                //_Test_Highlight_Graph_Components ();

                res = FallHangingPeaces ();
            }

            destroyTimer = 0;
            isAttachedCubesChecked = false;

            // Clear attachedCubes chain if some cubes were deleted
            for (int i = attachFigure.Count - 1; i >= 0; i--)
            {
                if ( ( attachFigure[i] == null ) || (!attachFigure[i].gameObject.activeSelf) )
                    attachFigure.RemoveAt (i);
            }

            // if we don't need to return (there weren't any hanging parts)
            // then we need to clear attachedCubes list
            if (!res)
                attachedCubes.Clear ();

            //foreach (CubeAttributes cubeAttr in attachFigure)
            //    Debug.Log ("Cube Attr 2 " + cubeAttr.ToString() );
        }
        return res;
    }

    void FindSameColorNeighbours (CubeAttributes cubeAttr)
    {
        RaycastHit hit;

        AddCubeToChain (cubeAttr);

        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.right, Vector3.right, out hit, CubePointPosition.size / 2.0f))
        {
            if (cubeAttr.color == hit.transform.GetComponent<CubeAttributes> ().color)
            {                
                AddCubeToChain (hit.transform.GetComponent<CubeAttributes> ());
                Debug.Log ("Right " + cubeAttr.color.ToString() + " cube added");
            }
        }
             
        if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.right, -Vector3.right, out hit, CubePointPosition.size / 2.0f))
        {
            if (cubeAttr.color == hit.transform.GetComponent<CubeAttributes> ().color)
            {
                AddCubeToChain (hit.transform.GetComponent<CubeAttributes> ());
                Debug.Log ("Left " + cubeAttr.color.ToString() + " cube added");
            }
        }

        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.up, Vector3.up, out hit, CubePointPosition.size / 2.0f))
        {
            if (cubeAttr.color == hit.transform.GetComponent<CubeAttributes> ().color)
            {
                AddCubeToChain (hit.transform.GetComponent<CubeAttributes> ());
                Debug.Log ("Up " + cubeAttr.color.ToString() + " cube added");
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.up, -Vector3.up, out hit, CubePointPosition.size / 2.0f))
        {
            if (cubeAttr.color == hit.transform.GetComponent<CubeAttributes> ().color)
            {
                AddCubeToChain (hit.transform.GetComponent<CubeAttributes> ());
                Debug.Log ("Down " + cubeAttr.color.ToString() + " cube added");
            }
        }

        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.forward, Vector3.forward, out hit, CubePointPosition.size / 2.0f))
        {
            if (cubeAttr.color == hit.transform.GetComponent<CubeAttributes> ().color)
            {
                AddCubeToChain (hit.transform.GetComponent<CubeAttributes> ());
                Debug.Log ("Forward " + cubeAttr.color.ToString() + " cube added");
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.forward, -Vector3.forward, out hit, CubePointPosition.size / 2.0f))
        {
            if (cubeAttr.color == hit.transform.GetComponent<CubeAttributes> ().color)
            {
                AddCubeToChain (hit.transform.GetComponent<CubeAttributes> ());
                Debug.Log ("Backward " + cubeAttr.color.ToString() + " cube added");
            }
        }
    }

    void AddCubeToChain (CubeAttributes cubeAttr)
    {
        if (!cubeAttr.isInChain) 
        {
            if ( colorScheme.colors[0] == cubeAttr.color )
            {
                firstColorChain.Add (cubeAttr);
            }
            else
            if ( colorScheme.colors[1] == cubeAttr.color )
            {
                secondColorChain.Add (cubeAttr);
            }
            else
            if ( colorScheme.colors[2] == cubeAttr.color )
            {
                thirdColorChain.Add (cubeAttr);
            }
            cubeAttr.isInChain = true;
        }
    }

    void DestroyColorChain (List<CubeAttributes> chain, bool count = false, bool sound = false)
    {
        bool wasGold = false;

        foreach (CubeAttributes cubeAttr in chain)
        {
            switch (cubeAttr.type)
            {
                case CubeType.gold:
                    wasGold = true;
                    if (count)
                        destroyedGoldCubes += 1;
                    break;
                case CubeType.regular:
                    if (count)
                        destroyedRegularCubes += 1;
                    break;
            }                

            cubeAttr.gameObject.SetActive (false);
            GameObject.Destroy (cubeAttr.gameObject);
            //Debug.Log ("DestroyColorChain cube attr " + cubeAttr.ToString());
        }

        //Debug.Log ("Sound " + sound.ToString() + "Gold " + wasGold.ToString() );
        if (sound)
        {
            if (wasGold)
                soundEffects.PlayingDesctructingGoldCube ();
            else
                soundEffects.PlayingDesctructingRegularCube ();
        }

        if (goldCubesLeftLabel)
            goldCubesLeftLabel.SetText ( (totalGoldCubes - destroyedGoldCubes).ToString () );
    }

    void EmptyColorChains ()
    {
        foreach (CubeAttributes cubeAttr in firstColorChain)
            cubeAttr.isInChain = false;
        foreach (CubeAttributes cubeAttr in secondColorChain)
            cubeAttr.isInChain = false;
        foreach (CubeAttributes cubeAttr in thirdColorChain)
            cubeAttr.isInChain = false;

        firstColorChain.Clear ();
        secondColorChain.Clear ();
        thirdColorChain.Clear ();

        crackedChain.Clear ();
    }

    void CalculateScore ()
    {
        float score = 0;
        short flag = 0;

        if (firstColorChain.Count > 2)
        {
            score += (firstColorChain.Count * 100.0f + (firstColorChain.Count - 3) * 100.0f);
            flag ++;
            Debug.Log ("Red +" + score);
        }
        if (secondColorChain.Count > 2)
        {
            score += (secondColorChain.Count * 100.0f + (secondColorChain.Count - 3) * 100.0f);
            Debug.Log ("White +" + (secondColorChain.Count * 100.0f + (secondColorChain.Count - 3) * 100.0f));
            flag ++;
        }
        if (thirdColorChain.Count > 2)
        {
            score += (thirdColorChain.Count * 100.0f + (thirdColorChain.Count - 3) * 100.0f);
            Debug.Log ("Yellow +" + (thirdColorChain.Count * 100.0f + (thirdColorChain.Count - 3) * 100.0f));
            flag ++;
        }

        if (flag > 2)
        {
            score *= 2.0f;
            Debug.Log ("3 chains bonus +" + score);
        }
        else
        if (flag > 1)
        {
            score *= 1.5f;
            Debug.Log ("2 chains bonus +" + 0.5f * score);
        }

        StaticLogic.AddScore (score);
    }

    public void CheckGraphContinuety ()
    {
        int i;
        CubeAttributes[] cubes;

        numberOfGraphComponenets = 1;

        cubes = transform.GetComponentsInChildren<CubeAttributes> ();
        Debug.Log ("CheckGraphContinuety Total cubes: " + cubes.Length);

        foreach (CubeAttributes ca in cubes)
            ca.graphComponent = 0;

        cubesInGraphComponent = 0;

        i = 0;
        while (cubesInGraphComponent < cubes.Length)
        {
            // we find the first unmark (graphComponent=0) cube
            while (cubes [i].graphComponent != 0)
                i ++;

            Debug.Log ("Graph -comp: " + numberOfGraphComponenets);
            MarkGraphComponent (cubes [i], numberOfGraphComponenets);
            Debug.Log ("Graph comp: " + numberOfGraphComponenets + " cubes: " + cubesInGraphComponent);
            numberOfGraphComponenets ++;
        }

        numberOfGraphComponenets --;
    }

    void MarkGraphComponent (CubeAttributes cubeAttr, int graphComponentIndex)
    {
        RaycastHit hit;

        cubeAttr.graphComponent = graphComponentIndex;
        cubesInGraphComponent ++;

        hangingFigure.Add (cubeAttr);

        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.right, Vector3.right, out hit, CubePointPosition.size / 2.0f))
        {
            if (hit.transform.GetComponent<CubeAttributes> ().graphComponent == 0)
            {
                MarkGraphComponent (hit.transform.GetComponent<CubeAttributes> (), graphComponentIndex);
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.right, -Vector3.right, out hit, CubePointPosition.size / 2.0f))
        {
            if (hit.transform.GetComponent<CubeAttributes> ().graphComponent == 0)
            {
                MarkGraphComponent (hit.transform.GetComponent<CubeAttributes> (), graphComponentIndex);
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.up, Vector3.up, out hit, CubePointPosition.size / 2.0f))
        {
            if (hit.transform.GetComponent<CubeAttributes> ().graphComponent == 0)
            {
                MarkGraphComponent (hit.transform.GetComponent<CubeAttributes> (), graphComponentIndex);
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.up, -Vector3.up, out hit, CubePointPosition.size / 2.0f))
        {
            if (hit.transform.GetComponent<CubeAttributes> ().graphComponent == 0)
            {
                MarkGraphComponent (hit.transform.GetComponent<CubeAttributes> (), graphComponentIndex);
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.forward, Vector3.forward, out hit, CubePointPosition.size / 2.0f))
        {
            if (hit.transform.GetComponent<CubeAttributes> ().graphComponent == 0)
            {
                MarkGraphComponent (hit.transform.GetComponent<CubeAttributes> (), graphComponentIndex);
            }
        }
        
        if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.forward, -Vector3.forward, out hit, CubePointPosition.size / 2.0f))
        {
            if (hit.transform.GetComponent<CubeAttributes> ().graphComponent == 0)
            {
                MarkGraphComponent (hit.transform.GetComponent<CubeAttributes> (), graphComponentIndex);
            }
        }
    }

    // if hanging pieces doesn't have another cubes to fall they disappear
    bool FallHangingPeaces ()
    {
        int i, j;
        List<KeyValuePair<int,float>> dist2base;
        RaycastHit hit;
        float score;
        List<CubeAttributes> currentChain;
        bool res = false;
        bool wasBaseCube = false;
        bool wasFall = false;

        currentChain = new List<CubeAttributes> ();

        dist2base = new List<KeyValuePair<int,float>>();

        Debug.Log ("FallHangingPeaces Total hanging cubes: " + hangingFigure.Count);

        for (i = 1; i <= numberOfGraphComponenets; i++)
        {
            Debug.Log ("FallHangingPeaces GC: " + i);

            wasBaseCube = false;

            // Find distance to base from each cube of Graph Component i
            for (j = 0; j < hangingFigure.Count; j++)
            {
                if (hangingFigure [j].graphComponent == i)
                {
                    currentChain.Add (hangingFigure [j]);
                    if ( hangingFigure [j].isBase )
                        wasBaseCube = true;
                    if (Physics.Raycast (hangingFigure [j].transform.position + 0.5f * CubePointPosition.size * Camera.main.transform.forward, Camera.main.transform.forward, out hit, 100f))
                    {
                        if (hit.transform.GetComponent<CubeAttributes> ().graphComponent != i )
                        {
                            dist2base.Add (new KeyValuePair<int, float> (j, hit.distance));
                            //Debug.Log ("FallHangingPeaces added distance: " + dist2base[dist2base.Count - 1].Value);
                        }
                    }
                }
            }

            // if there was a base cube bellow then we move figure for the smallest distance
            if (dist2base.Count > 0)
            {
                wasFall = true;
                dist2base.Sort (
                    delegate (KeyValuePair<int, float> firstPair, KeyValuePair<int, float> nextPair)
                    {
                        return firstPair.Value.CompareTo(nextPair.Value);
                    }
                );
                Debug.Log ("FallHangingPeaces distance to base: " + dist2base[0].Value);
                hangingFigure [ dist2base[0].Key ].SetGivenColor( Color.green );
                MoveChainOfCubes (currentChain, CubePointPosition.size * Mathf.Round (dist2base[0].Value), Camera.main.transform.forward);
                //foreach (CubeAttributes cubeAttr in currentChain)
                //    attachedCubes.Add (cubeAttr);
            }
            else
            if (!wasBaseCube)
            {
                score = currentChain.Count * 100;
                StaticLogic.AddScore (score);
                Debug.Log ("FallHangingPeaces GC: " + i + " deleted");
                DestroyColorChain (currentChain);
            }
            currentChain.Clear ();
            dist2base.Clear ();
        }

        numberOfGraphComponenets = 1;

        res = wasFall;

        hangingFigure.Clear ();

        Debug.Log ("FallHangingPeaces end falling");
        return res;
    }

    void MoveChainOfCubes (List<CubeAttributes> chain, float distance, Vector3 dir)
    {
        foreach (CubeAttributes cubeAttr in chain)
        {
            //Debug.Log ("Moved from " + cubeAttr.transform.position.ToString () + " by " + (dir * distance).ToString ());
            cubeAttr.transform.Translate (dir * distance, Space.World);
            cubeAttr.UpdateLevel ();
            //Debug.Log ("Moved to " + cubeAttr.transform.position.ToString ());
        }

        soundEffects.PlayingAttachingRegularCube ();
    }

    void HighlightChains (int regime = 0)
    {
        Color c;
        Transform temp;

        if (regime == 0)
        {
            if (firstColorChain.Count > 2)
                foreach (CubeAttributes cubeAttr in firstColorChain)
                {
                    temp = Instantiate(crackedCube, cubeAttr.transform.position, 
                                       Quaternion.identity ) as Transform;
                                       
                    temp.GetComponent<CubeAttributes>().Init (cubeAttr.color, cubeAttr.coords, cubeAttr.type);
                    
                    temp.GetComponent<CenterCube>().changeColor (cubeAttr.color);

                    crackedChain.Add (temp.GetComponent<CubeAttributes>());

                    cubeAttr.gameObject.SetActive (false);
                }

            if (secondColorChain.Count > 2)
                foreach (CubeAttributes cubeAttr in secondColorChain)
                {
                    temp = Instantiate(crackedCube, cubeAttr.transform.position, 
                                       Quaternion.identity ) as Transform;
                                       
                    temp.GetComponent<CubeAttributes>().Init (cubeAttr.color, cubeAttr.coords, cubeAttr.type);
                    
                    temp.GetComponent<CenterCube>().changeColor (cubeAttr.color);

                    crackedChain.Add (temp.GetComponent<CubeAttributes>());

                    cubeAttr.gameObject.SetActive (false);
                }
        
            if (thirdColorChain.Count > 2)
                foreach (CubeAttributes cubeAttr in thirdColorChain)
                {
                    temp = Instantiate(crackedCube, cubeAttr.transform.position, 
                                       Quaternion.identity ) as Transform;
                                       
                    temp.GetComponent<CubeAttributes>().Init (cubeAttr.color, cubeAttr.coords, cubeAttr.type);
                    
                    temp.GetComponent<CenterCube>().changeColor (cubeAttr.color);

                    crackedChain.Add (temp.GetComponent<CubeAttributes>());

                    cubeAttr.gameObject.SetActive (false);
                }
        }
        // TODO: Remake for color independence
        if (regime == 1)        
        {
            if (firstColorChain.Count > 2)
                foreach (CubeAttributes cubeAttr in firstColorChain)
                {
                    c = new Color (1.0f, 128.0f/255.0f, 192.0f/255.0f);
                    cubeAttr.transform.GetComponent<Renderer>().material.color = c;
                }

            if (secondColorChain.Count > 2)
                foreach (CubeAttributes cubeAttr in secondColorChain)
                {
                    c = new Color (1.0f, 212.0f/255.0f, 0.0f);
                    cubeAttr.transform.GetComponent<Renderer>().material.color = c;
                }
        
            if (thirdColorChain.Count > 2)
                foreach (CubeAttributes cubeAttr in thirdColorChain)
                {
                    c = new Color (128.0f/255.0f, 1.0f, 1.0f);
                    cubeAttr.transform.GetComponent<Renderer>().material.color = c;
                }
        }
    }

    // Works only for first 3 colors (unfinished)
    public void CalculateCubesForProbabilities (int size_x, int size_y, ref int N1, ref int N2, ref int N3)
    {
        int startXPos;
        int startYPos;

        startXPos = - Mathf.RoundToInt ( (float)( size_x - 1 ) / 2.0f );
        startYPos = - Mathf.RoundToInt ( (float)( size_y - 1 ) / 2.0f );

        for ( int i = startXPos; i < size_x; i++ )
            for ( int j = startXPos; j < size_x; j++ )
            {

            }
    }

    bool FallSingleCubes ()
    {
        RaycastHit hit;
        bool res = false;

        //Debug.Log ("FallSingleCubes start");

        if (firstColorChain.Count == 1)
        {
            Debug.Log ("FallSingleCubes 1st color");
            if ( ( IsCubeSeparated (firstColorChain [0]) ) && ( Physics.Raycast (firstColorChain [0].transform.position + 0.5f * CubePointPosition.size * Camera.main.transform.forward, Camera.main.transform.forward, out hit, 100f) ) )
            {
                if (hit.distance >= 0.5f * CubePointPosition.size)
                {
                    MoveChainOfCubes (firstColorChain, CubePointPosition.size * Mathf.Round (hit.distance), Camera.main.transform.forward);
                    attachedCubes.Add (firstColorChain [0]);
                    res = true;
                }
            }
        }

        if (secondColorChain.Count == 1)
        {
            Debug.Log ("FallSingleCubes 2nd color");
            if ( ( IsCubeSeparated (secondColorChain [0]) ) && ( Physics.Raycast (secondColorChain [0].transform.position + 0.5f * CubePointPosition.size * Camera.main.transform.forward, Camera.main.transform.forward, out hit, 100f) ) )
            {
                if (hit.distance >= 0.5f * CubePointPosition.size)
                {
                    MoveChainOfCubes (secondColorChain, CubePointPosition.size * Mathf.Round (hit.distance), Camera.main.transform.forward);
                    attachedCubes.Add (secondColorChain [0]);
                    res = true;
                }
            }
        }
        if (thirdColorChain.Count == 1)
        {
            Debug.Log ("FallSingleCubes 3rd color");
            if ( ( IsCubeSeparated (thirdColorChain [0]) ) && ( Physics.Raycast (thirdColorChain [0].transform.position + 0.5f * CubePointPosition.size * Camera.main.transform.forward, Camera.main.transform.forward, out hit, 100f) ) )
            {
                if (hit.distance >= 0.5f * CubePointPosition.size)
                {
                    MoveChainOfCubes (thirdColorChain, CubePointPosition.size * Mathf.Round (hit.distance), Camera.main.transform.forward);
                    attachedCubes.Add (thirdColorChain [0]);
                    res = true;
                }
            }
        }

        return res;
    }

    bool IsCubeSeparated (CubeAttributes cubeAttr)
    {
        RaycastHit hit;
        bool res = false;

        if ( !(Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.right, Vector3.right, out hit, CubePointPosition.size / 2.0f)) )             
            if ( !(Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.right, -Vector3.right, out hit, CubePointPosition.size / 2.0f)) )
                if ( !(Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.up, Vector3.up, out hit, CubePointPosition.size / 2.0f)) )
                    if ( !(Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.up, -Vector3.up, out hit, CubePointPosition.size / 2.0f)) )
                        if ( !(Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.forward, Vector3.forward, out hit, CubePointPosition.size / 2.0f)) )
                            if ( !(Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.forward, -Vector3.forward, out hit, CubePointPosition.size / 2.0f)) )
                                res = true;
        return res;
    }

    public bool IsGoldNeighbour (CubeAttributes cubeAttr)
    {
        RaycastHit hit;
        bool res = false;

        if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.right, Vector3.right, out hit, CubePointPosition.size / 2.0f))
            if ( hit.transform.GetComponent<CubeAttributes> ().type == CubeType.gold )
                res = true;

        if (!res)
            if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.right, -Vector3.right, out hit, CubePointPosition.size / 2.0f))
                if ( hit.transform.GetComponent<CubeAttributes> ().type == CubeType.gold )
                    res = true;
        if (!res)
            if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.up, Vector3.up, out hit, CubePointPosition.size / 2.0f))
                if ( hit.transform.GetComponent<CubeAttributes> ().type == CubeType.gold )
                    res = true;

        if (!res)
            if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.up, -Vector3.up, out hit, CubePointPosition.size / 2.0f))
                if ( hit.transform.GetComponent<CubeAttributes> ().type == CubeType.gold )
                    res = true;

        if (!res)
            if (Physics.Raycast (cubeAttr.transform.position + CubePointPosition.size / 2.0f * Vector3.forward, Vector3.forward, out hit, CubePointPosition.size / 2.0f))
                if ( hit.transform.GetComponent<CubeAttributes> ().type == CubeType.gold )
                    res = true;

        if (!res)
            if (Physics.Raycast (cubeAttr.transform.position - CubePointPosition.size / 2.0f * Vector3.forward, -Vector3.forward, out hit, CubePointPosition.size / 2.0f))
                if ( hit.transform.GetComponent<CubeAttributes> ().type == CubeType.gold )
                    res = true;

        return res;
    }

    void _Test_HighlightChains (int regime)
    {
        Color c;

        Debug.Log ("Red Chain: " + firstColorChain.Count.ToString ());
        Debug.Log ("White Chain: " + secondColorChain.Count.ToString ());
        Debug.Log ("Yellow Chain: " + thirdColorChain.Count.ToString ());

        if (regime == 1)
        {
            foreach (CubeAttributes cubeAttr in firstColorChain)
            {
                c = new Color (1.0f, 128.0f/255.0f, 192.0f/255.0f);
                cubeAttr.transform.GetComponent<Renderer>().material.color = c;
            }

            foreach (CubeAttributes cubeAttr in secondColorChain)
            {
                c = new Color (1.0f, 212.0f/255.0f, 0.0f);
                cubeAttr.transform.GetComponent<Renderer>().material.color = c;
            }
        
            foreach (CubeAttributes cubeAttr in thirdColorChain)
            {
                c = new Color (128.0f/255.0f, 1.0f, 1.0f);
                cubeAttr.transform.GetComponent<Renderer>().material.color = c;
            }
        }
    }

    void _Test_Highlight_Graph_Components ()
    {
        CubeAttributes[] cubes;
        Color c = Color.green;

        cubes = transform.GetComponentsInChildren<CubeAttributes> ();

        foreach (CubeAttributes cubeAttr in cubes)
        {
            switch (cubeAttr.graphComponent % 5)
            {
            case 0:
                c = Color.red;
                break;
            case 1:
                c = Color.green;
                break;
            case 2:
                c = Color.blue;
                break;
            case 3:
                c = Color.cyan;
                break;
            case 4:
                c = Color.yellow;
                break;
            }
            
            cubeAttr.SetGivenColor (c);
        }
    }
}
