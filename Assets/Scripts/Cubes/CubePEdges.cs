﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePEdges : CubeEdges
{
    public float thickness = 0.02f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void setColor(CubeColors col)
    {
        Color color;

        this.GetComponent<CubeAttributes>().color = col;
        
        color = colors[col];
        foreach (Transform child2 in transform)
            foreach (Transform child in child2)
                child.transform.GetComponent<Renderer>().material.SetColor("_Color", color);
    }

    public override void setWidth(float width)
    {
        this.setWidth (width);
    }

    public override void setWidth(float width, float indent = 0)
    {
        thickness = width;

        // for safe sclaing later
        if ( indent > 0.45f )
            indent = 0.45f;
        
        foreach (Transform child2 in transform)
        {
            foreach (Transform child in child2)
            {
                child.transform.localScale = new Vector3 ( thickness, thickness, 1 - 2.0f * indent );
            }

            child2.GetChild (0).transform.localPosition = new Vector3 ( 0.5f - thickness/2 - indent, 0.5f - thickness/2 - indent, 0 );

            child2.GetChild (1).transform.localPosition = new Vector3 ( -0.5f + thickness/2 + indent, 0.5f - thickness/2 - indent, 0 );

            child2.GetChild (2).transform.localPosition = new Vector3 ( 0.5f - thickness/2 - indent, -0.5f + thickness/2 + indent, 0 );

            child2.GetChild (3).transform.localPosition = new Vector3 ( -0.5f + thickness/2 + indent, -0.5f + thickness/2 + indent, 0 );
        }
    }
}
