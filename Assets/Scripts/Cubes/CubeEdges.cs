﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeEdges : MonoBehaviour
{
    public Color color = Color.white;
    
    // colors according to CubeColors
    protected static Dictionary<CubeColors,Color> colors;
    
    // Start is called before the first frame update
    void Awake()
    {
        colors = new Dictionary<CubeColors,Color> ();
        colors.Add ( CubeColors.red, new Color (255.0f/255.0f, 113.0f/255.0f, 84.0f/255.0f, 1f) );
        colors.Add ( CubeColors.white, new Color (1f, 1f, 1f, 1f) );
        colors.Add ( CubeColors.yellow, new Color (255.0f/255.0f, 255.0f/255.0f, 77.0f/255.0f, 1f) );
        colors.Add ( CubeColors.blue, new Color (23.0f/255.0f, 140.0f/255.0f, 255.0f/255.0f, 1f) );
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public virtual void setColor(CubeColors col)
    {
        this.GetComponent<CubeAttributes>().color = col;
        
        color = colors[col];
        foreach (Transform child in transform)
            child.GetComponent<LineRenderer>().startColor = child.GetComponent<LineRenderer>().endColor = color;
    }

    public virtual void setWidth(float width)
    {
        for ( int i = 0; i < 4 ; i++ )
            this.transform.GetChild (i).GetComponent<LineRenderer>().startWidth = this.transform.GetChild (i).GetComponent<LineRenderer>().endWidth = width;
    }

    public virtual void setWidth(float width, float indent)
    {
        this.setWidth (width);
    }
}
