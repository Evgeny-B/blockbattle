using System;
using UnityEngine;

public enum CubeColors {none, red, white, yellow, purple, blue};

public enum CubeType {regular, gold};

public class CubeCoords 
{
    public float x;
    public float y;
    public float z;
    
    public CubeCoords(float x_, float y_, float z_)
    {
        x = x_;
        y = y_;
        z = z_;
    }
    
    public static int returnLevel(float x_, float y_, float z_, float center_x, float center_y, float center_z)
    {
        return Mathf.Max ( Mathf.Abs ( Mathf.RoundToInt ( x_ - center_x ) ), 
                          Mathf.Max ( Mathf.Abs ( Mathf.RoundToInt ( y_ - center_y ) ), 
                                     Mathf.Abs ( Mathf.RoundToInt ( z_ - center_z ) ) 
                                   )
                        );
    }

    public static int returnLevelAlongAxis(float x_, float center_x)
    {
        float fval = ( x_ - center_x );
        int ival = Mathf.RoundToInt ( x_ - center_x );
        int res;
        // this if need when center of cubes shifted by 0.5
        if ( Mathf.Abs ( fval - (float) ival ) > 0.1f )
        {
            if ( x_ > 0 )
                res = ( Mathf.RoundToInt ( x_ - 0.5f - center_x ) );
            else
                res = ( Mathf.RoundToInt ( x_ + 0.5f - center_x ) );
        }
        else
            res = ( Mathf.RoundToInt ( x_ - center_x ) );
        return res;
    }
    
    public int returnLevelForCurrentCube()
    {
        return CubeCoords.returnLevel(x, y, z, 0, 0, 0);
    }

    public void calcXYZLevelForCurrentCube(ref int lvl_x, ref int lvl_y, ref int lvl_z)
    {
        lvl_x = CubeCoords.returnLevelAlongAxis(x, 0);
        //Debug.Log (x + " x level: " + lvl_x);
        lvl_y = CubeCoords.returnLevelAlongAxis(y, 0);
        //Debug.Log (y + " y level: " + lvl_y);
        lvl_z = CubeCoords.returnLevelAlongAxis(z, 0);
        //Debug.Log (z + " z level: " + lvl_z);
    }

    public override string ToString ()
    {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}

public class CubeAttributes : MonoBehaviour
{
	public CubeColors color;
	public bool isInChain; // determines if this cube was already added to the chain for deleting
	public int graphComponent; // determines to which graph component this cube belongs. Differ if graph is not tie-up.
    public CubeCoords coords; // coordinates of cube. Point of origin in (halfFieldSize,halfFieldSize,halfFieldSize). Unit is CubePointPosition.size 
    public CubeType type; // regular or gold
    public bool isBase; // show that this cube was initially created at base figure

    public int level; // distance to the center cube
    public int calibrate; // for color calibrating
    
    // distance to the center cube in corresponding axis
    public int level_x;
    public int level_y;
    public int level_z;

	public void Init (CubeColors col, CubeCoords coords_, CubeType type_ = CubeType.regular, int calibrate_ = 0, bool isBase_ = false)
	{
		color = col;
		isInChain = false;
		graphComponent = 0;
        coords = coords_;
        //Debug.Log ( "coords ( " + coords.x + ", " + coords.y + ", " + coords.z + " )");
        type = type_;
        calibrate = calibrate_;
        isBase = isBase_;
        level = coords.returnLevelForCurrentCube();
        coords.calcXYZLevelForCurrentCube ( ref level_x, ref level_y, ref level_z );
        SetColor();
        //Debug.Log ( level + " ( " + level_x + ", " + level_y + ", " + level_z + " )");
	}

    public void UpdateLevel ()
    {
        Vector3 tmp;

        tmp = ( transform.position - StaticLogic.baseFigure.transform.position ) / CubePointPosition.size;
        
        coords = new CubeCoords ( tmp.x, tmp.y, tmp.z );

        //Debug.Log ( "Update cube coords: " + tmp.ToString () );

        level = coords.returnLevelForCurrentCube ();

        coords.calcXYZLevelForCurrentCube ( ref level_x, ref level_y, ref level_z );

        SetColor();
    }

    void SetColor ()
    {
        int lvl;
        string[] cubeLevelColors = CubeColorsArrays.redColors;
        Color cubeColor;

        // TODO: make Class interface instead of IF
        if ( type == CubeType.regular )
        {
            cubeLevelColors = StaticLogic.cubeColorsArrays[color];

            if ( ( StaticLogic.gameLogic != null ) && (StaticLogic.gameLogic.isCycleColors) )
            {
                lvl = Mathf.Abs ( level_z ) % 2;
            }
            else
            {
                lvl = Mathf.Abs ( level_z ) + calibrate;
                if (lvl >= cubeLevelColors.Length)
                    lvl = cubeLevelColors.Length - 1;
            }

            ColorUtility.TryParseHtmlString (cubeLevelColors[lvl], out cubeColor);

            if ( ( StaticLogic.gameLogic != null ) && (StaticLogic.gameLogic.isOneColor) )
            {
                // Debug.Log ( "Color " + color );
                switch (color)
                {
                    case CubeColors.red:
                        ColorUtility.TryParseHtmlString ("#FF7154", out cubeColor);
                        break;
                    case CubeColors.white:
                        ColorUtility.TryParseHtmlString ("#FFFFFF", out cubeColor);
                        break;
                    case CubeColors.yellow:
                        ColorUtility.TryParseHtmlString ("#FFFF4D", out cubeColor);
                        break;
                }            
            }

            // Debug.Log ( "Color " + cubeColor );

            transform.GetComponent<Renderer>().material.SetColor("_Color", cubeColor);
        }
        else
        {
            transform.GetComponent<CenterCube>().changeColor (color);
        }
    }

    public void SetGivenColor (Color color)
    {
        if ( type == CubeType.regular )
        {
            transform.GetComponent<Renderer>().material.SetColor("_Color", color);
        }
    }
}

