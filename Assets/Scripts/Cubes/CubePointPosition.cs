using UnityEngine;

public class CubePointPosition
{
	public static float size = 1.03f;
	
	public int i;  // index in figure array
	
	public CubePointPosition xLeft_;
	public CubePointPosition xRight_;
	public CubePointPosition yLeft_;
	public CubePointPosition yRight_;
	public CubePointPosition zLeft_;
	public CubePointPosition zRight_;

	public CubePointPosition xLeft
	{
		get { return xLeft_; }
	}
	public CubePointPosition xRight
	{
		get { return xRight_; }
	}
	public CubePointPosition yLeft
	{
		get { return yLeft_; }
	}
	public CubePointPosition yRight
	{
		get { return yRight_; }
	}
	public CubePointPosition zLeft
	{
		get { return zLeft_; }
	}
	public CubePointPosition zRight
	{
		get { return zRight_; }
	}

	public CubePointPosition (int i_)
	{
		i = i_;
	}

	public void SetOneNeighbourAND (EDirection dir, CubePointPosition cube)
	{
		switch (dir)
		{
		case EDirection.backward:
			xLeft_ = null;
			xRight_ = null;
			yLeft_ = null;
			yRight_ = null;
			zLeft_ = cube;
			zRight_ = null;
			break;
		case EDirection.forward:
			xLeft_ = null;
			xRight_ = null;
			yLeft_ = null;
			yRight_ = null;
			zLeft_ = null;
			zRight_ = cube;
			break;
		case EDirection.left:
			xLeft_ = cube;
			xRight_ = null;
			yLeft_ = null;
			yRight_ = null;
			zLeft_ = null;
			zRight_ = null;
			break;
		case EDirection.right:
			xLeft_ = null;
			xRight_ = cube;
			yLeft_ = null;
			yRight_ = null;
			zLeft_ = null;
			zRight_ = null;
			break;
		case EDirection.down:
			xLeft_ = null;
			xRight_ = null;
			yLeft_ = cube;
			yRight_ = null;
			zLeft_ = null;
			zRight_ = null;
			break;
		case EDirection.up:
			xLeft_ = null;
			xRight_ = null;
			yLeft_ = null;
			yRight_ = cube;
			zLeft_ = null;
			zRight_ = null;
			break;
		}
	}

	public void SetOneNeighbourOR (EDirection dir, CubePointPosition cube)
	{
		switch (dir)
		{
		case EDirection.backward:
			zLeft_ = cube;
			break;
		case EDirection.forward:
			zRight_ = cube;
			break;
		case EDirection.left:
			xLeft_ = cube;
			break;
		case EDirection.right:
			xRight_ = cube;
			break;
		case EDirection.down:
			yLeft_ = cube;
			break;
		case EDirection.up:
			yRight_ = cube;
			break;
		}
	}
}

