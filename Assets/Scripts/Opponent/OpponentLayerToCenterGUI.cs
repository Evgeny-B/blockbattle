﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OpponentLayerToCenterGUI : MonoBehaviour
{
    public OpponentPlayer opponent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (opponent != null)
            transform.GetComponent<TextMeshProUGUI> ().SetText ( opponent.layersToCenter.ToString () );
    }
}
