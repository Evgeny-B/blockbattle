﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentPlayer : MonoBehaviour
{
    public float initialDelay = 3.0f; // Seconds. Delay before algorithm will start work 
    public float minRandomDelay = 3.0f; // Seconds. Min delay before algorithm will make next turn 
    public float maxRandomDelay = 10.0f; // Seconds. Max delay before algorithm will make next turn 
    public int layersToCenter; // Maximum cube level over the center cube 
    public int destroyedLayers; // How many layers opponent destroyed

    public int destroyedCubes; // Number of destroyed cubes
    int addedCubes; // Number of cubes added to base

    List<float> prob3Cubes; // Probabilities that 3 cubes will be destroyed
    List<float> prob4Cubes; // Probabilities that 4 cubes will be destroyed
    List<float> prob5Cubes; // Probabilities that 5 cubes will be destroyed
    List<float> prob6Cubes; // Probabilities that 6 cubes will be destroyed

    List<int> layerLadder; // when to decrease level

    List<int> sampledColorCubesAmount; // Random numbers of cubes of each colors. Sampled each time

    float initDelayTimer;
    float randomDelayTimer;
    float currentRandomDelay;

    // Start is called before the first frame update
    void Start()
    {
        prob3Cubes = new List<float>();
        prob3Cubes.Add (0);
        prob3Cubes.Add (0);
        prob3Cubes.Add (0.1f);
        prob3Cubes.Add (0.15f);
        prob3Cubes.Add (0.5f);
        prob3Cubes.Add (0.75f);
        prob3Cubes.Add (0.75f);
        prob3Cubes.Add (0.90f);
        prob3Cubes.Add (0.95f);
        prob3Cubes.Add (1.0f);

        prob4Cubes = new List<float>();
        prob4Cubes.Add (0);
        prob4Cubes.Add (0);
        prob4Cubes.Add (0);
        prob4Cubes.Add (0);
        prob4Cubes.Add (0);
        prob4Cubes.Add (0.25f);
        prob4Cubes.Add (0.25f);
        prob4Cubes.Add (0.50f);
        prob4Cubes.Add (0.70f);
        prob4Cubes.Add (0.80f);

        prob5Cubes = new List<float>();
        prob5Cubes.Add (0);
        prob5Cubes.Add (0);
        prob5Cubes.Add (0);
        prob5Cubes.Add (0);
        prob5Cubes.Add (0);
        prob5Cubes.Add (0);
        prob5Cubes.Add (0.1f);
        prob5Cubes.Add (0.25f);
        prob5Cubes.Add (0.50f);
        prob5Cubes.Add (0.60f);

        prob6Cubes = new List<float>();
        prob6Cubes.Add (0);
        prob6Cubes.Add (0);
        prob6Cubes.Add (0);
        prob6Cubes.Add (0);
        prob6Cubes.Add (0);
        prob6Cubes.Add (0);
        prob6Cubes.Add (0.05f);
        prob6Cubes.Add (0.1f);
        prob6Cubes.Add (0.25f);
        prob6Cubes.Add (0.50f);

        layerLadder = new List<int>();
        layerLadder.Add (3);
        layerLadder.Add (8);
        layerLadder.Add (21);
        layerLadder.Add (35);
        layerLadder.Add (65);

        sampledColorCubesAmount = new List<int>();
        sampledColorCubesAmount.Add (0);
        sampledColorCubesAmount.Add (0);
        sampledColorCubesAmount.Add (0);

        initDelayTimer = 0;
        randomDelayTimer = maxRandomDelay;

        destroyedCubes = 0;
        addedCubes = 0;

        currentRandomDelay = Random.Range( minRandomDelay, maxRandomDelay );

        layersToCenter = StaticLogic.baseFigure.pit.z / 2 - 1;
        destroyedLayers = 0;
    }

    // Update is called once per frame
    void Update()
    {
        int d1, d2; // Dividions for correct sampling;
//        float p0, p3, p4, p5; // probabilities: p0 - zero block delete, and 3,4,5 blocks delete
        int destroyed;
        float sampledProb;
        int i, j;

        if ( initDelayTimer >= initialDelay )
        {
            if ( randomDelayTimer >= currentRandomDelay )
            {
                d1 = Random.Range(0, 10);
                d2 = Random.Range(0, 10);
                sampledColorCubesAmount [0] = Mathf.Min ( d1, d2 );
                sampledColorCubesAmount [2] = 9 - Mathf.Max ( d1, d2 );
                sampledColorCubesAmount [1] = 9 - sampledColorCubesAmount [2] - sampledColorCubesAmount [0];
                Debug.Log ( "Time: " + randomDelayTimer + " Sampled (" + sampledColorCubesAmount [0] + ", " + sampledColorCubesAmount [1] + ", " + sampledColorCubesAmount [2] + ")" );
                randomDelayTimer = 0;
                currentRandomDelay = Random.Range( minRandomDelay, maxRandomDelay);

                // TODO work only in easy regime
                j = Random.Range(0, 3); // exclude one color. figure sampling
                for ( i = 0; i < 3; i++ )
                    if (i != j)
                    {
                        destroyed = 0;  // this simulates adding cube to base
                        sampledProb = Random.Range ( 0.0f, 1.0f );
                        if ( sampledProb < prob6Cubes[ sampledColorCubesAmount [i] ] )
                            destroyed = 6;
                        else if ( sampledProb < prob5Cubes[ sampledColorCubesAmount [i] ] )
                            destroyed = 5;
                        else if ( sampledProb < prob4Cubes[ sampledColorCubesAmount [i] ] )
                            destroyed = 4;
                        else if ( sampledProb < prob3Cubes[ sampledColorCubesAmount [i] ] )
                            destroyed = 3;
                        destroyedCubes += destroyed;

                        if ( destroyed == 0 )
                            addedCubes += 1;
                        // Debug.Log ( i + " destroyed: " + destroyed );
                        // Debug.Log ( i + " sampledProb: " + sampledProb );
                    }

                // deciding if we need to decrease layers left to center cube
                if ( destroyedCubes - addedCubes >= layerLadder [ destroyedLayers ] )
                    if ( destroyedLayers < layerLadder.Count - 1 )
                    {
                        destroyedLayers += 1;
                        if ( layersToCenter >= 0 )
                            layersToCenter -= 1;
                    }
                Debug.Log ( "layer to center " + layersToCenter );
            }
            else
                randomDelayTimer += Time.deltaTime;
        }
        else
            initDelayTimer += Time.deltaTime;

        if ( layersToCenter == -1 )
            Debug.Log ( "Opponent win" );
    }

    // Computer algo to find proper place
    void FindThePlace()
    {

    }

    // Move, rotate, and fall opponent figure to the place
    void PutFigureToThePlace()
    {

    }
}
