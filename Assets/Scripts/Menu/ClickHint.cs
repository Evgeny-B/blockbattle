﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickHint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.GetComponent<RawImage>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKeyDown ( KeyCode.Space ) )
        {
            transform.GetComponent<RawImage>().enabled = (true);
        }
    }
}
