﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromoCubeRotate : MonoBehaviour
{
    public float rotationSpeed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float da;

        da = -90.0f * Time.deltaTime * rotationSpeed;
        transform.RotateAround (transform.position, -Camera.main.transform.up, da);
    }
}
