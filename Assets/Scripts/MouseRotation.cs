﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseRotation : MonoBehaviour
{
    public float RotationSpeed = 100;

    void Start ()
    {
        transform.rotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position) * Quaternion.Euler(0, 180, 0);
    }

    // Update is called once per frame
    void Update () 
    {
        if (Input.GetMouseButton(0))
        {
            transform.Rotate( (Input.GetAxis("Mouse Y") * RotationSpeed * Time.deltaTime), (- Input.GetAxis("Mouse X") * RotationSpeed * Time.deltaTime), 0, Space.World );
        }
    }
}
