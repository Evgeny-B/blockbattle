﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowFigure : MonoBehaviour
{
	[SerializeField]
	Transform target;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = Vector3.zero;
        target = transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = target.position;
//        transform.Translate (transform.right * 5.0f * CubePointPosition.size);   
    }
}
