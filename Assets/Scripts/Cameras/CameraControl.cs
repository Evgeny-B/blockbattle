﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraControl : MonoBehaviour {
    public GameLogic gameLogic; // Connection to other game objects in the game
    
	//public float speed = 10.0f;	// TODO: What is it?
	//public float scrollSpeed = 2.0f; // TODO: What is it?
	public float rotationSpeed = 1.0f;

    public float cameraDistance = 10f; // in CubePointPosition.size

    public float timeToRotate = 1.0f;
    public float demoAnimationDuration = 1.5f;
    float timerToStartAnim = 0;
    float angleForStartAnimation = 0;
    bool isDemoCameraAnimStarted = false;
    public bool isDemoCameraAnimFinished = false; // Indicate when anim is finished

	float angle = 0;

	public ERotating rotating = ERotating.no;
	public EDirection direction;
	public float da;
    
    public GameObject lightSources;

    public enum ECameraControlInterface {turn_right, turn_left, turn_up, turn_down};

    public Dictionary<ECameraControlInterface,KeyCode> controlBinding;

	// Use this for initialization
	void Start () 
	{
        controlBinding = new Dictionary<ECameraControlInterface,KeyCode> ();
        controlBinding.Add ( ECameraControlInterface.turn_right, KeyCode.None );
        controlBinding.Add ( ECameraControlInterface.turn_left, KeyCode.None );
        controlBinding.Add ( ECameraControlInterface.turn_up, KeyCode.None );
        controlBinding.Add ( ECameraControlInterface.turn_down, KeyCode.None );

        setCameraDistance ( cameraDistance * CubePointPosition.size);

        if (gameLogic.isDemoCameraAnim)
        {
            setCameraRotation (ERotating.right);
            //Debug.Log ("Camera anim started");
        }
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (isDemoCameraAnimStarted)
        {
            timerToStartAnim = timerToStartAnim + Time.deltaTime;
            if ( timerToStartAnim >= timeToRotate )
            {
                //Debug.Log ("Camera anim rotating " + angle.ToString() );
                runDemoAnimation ();
            }
            if ( timerToStartAnim >= timeToRotate + demoAnimationDuration )
            {
                isDemoCameraAnimStarted = false;
                isDemoCameraAnimFinished = true;
                //Debug.Log ("Camera anim finished");
            }
        }

		RotateCamera (ref rotating, ref angle, rotationSpeed, Vector3.zero, transform);

		if (rotating == ERotating.no)
		{
			angle = 0;
			if ( Input.GetKey ( controlBinding [ ECameraControlInterface.turn_right ] ) )
				rotating = ERotating.right;
			else
			if ( Input.GetKey ( controlBinding [ ECameraControlInterface.turn_left ] ) )
					rotating = ERotating.left;
			else
			if ( Input.GetKey ( controlBinding [ ECameraControlInterface.turn_up ] ) )
				rotating = ERotating.up;
			else
			if ( Input.GetKey ( controlBinding [ ECameraControlInterface.turn_down ] ) )
				rotating = ERotating.down;
		}
	}

	public void RotateCamera (ref ERotating rotating, ref float angle, float rotationSpeed,
	                                 Vector3 zeroPoint, Transform transform)
	{
        //if (rotating != ERotating.no)
        //    Debug.Log ("Camera rotating" + rotating.ToString () );
		float da = 0;
        Vector3 axis = Vector3.zero;
        float newAngle = 0;
        float targetAngle = 0;

		switch (rotating)
		{
		case ERotating.right:
			da = 90.0f * Time.deltaTime * rotationSpeed;
            newAngle = angle + da;
            targetAngle = 90.0f - angle;
            axis = -transform.up;
			break;
		case ERotating.left:
			da = -90.0f * Time.deltaTime * rotationSpeed;
            newAngle = - angle - da;
            targetAngle = -90.0f - angle;
            axis = -transform.up;
			break;
		case ERotating.up:
			da = 90.0f * Time.deltaTime * rotationSpeed;
            newAngle = angle + da;
            targetAngle = 90.0f - angle;
            axis = transform.right;
			break;
		case ERotating.down:
			da = -90.0f * Time.deltaTime * rotationSpeed;
            newAngle = - angle - da;
            targetAngle = -90.0f - angle;
            axis = transform.right;
			break;
		}

        if (newAngle > 90.0f)
        {
            transform.RotateAround (zeroPoint, axis, targetAngle);
            // Rotate figure
            gameLogic.figure.transform.RotateAround (zeroPoint, axis, targetAngle);
            // Rotate light
            lightSources.transform.RotateAround (zeroPoint, axis, targetAngle);
            rotating = ERotating.no;
            gameLogic.figure.redrawSelection = true;
            angle = 0;
        }
        else
        {
            transform.RotateAround (zeroPoint, axis, da);
            // Rotate figure
            gameLogic.figure.transform.RotateAround (zeroPoint, axis, da);
            // Rotate light
            lightSources.transform.RotateAround (zeroPoint, axis, da);
            angle += da;
            gameLogic.figure.relativeDirection = StaticLogic.TurnDirection (Camera.main.transform.position);
        }
	}

    public void setCameraRotation (ERotating direction)
    {
        switch (direction)
        {
            case ERotating.right:
                transform.RotateAround (Vector3.zero, -transform.up, 90.0f);
                // Rotate light
                lightSources.transform.RotateAround (Vector3.zero, -transform.up, 90.0f); 
                break;
            case ERotating.left:
                transform.RotateAround (Vector3.zero, -transform.up, -90.0f);
                // Rotate light
                lightSources.transform.RotateAround (Vector3.zero, -transform.up, -90.0f); 
                break;
        }
    }

    public void setCameraDistance (float distance)
    {
        transform.Translate (new Vector3 (0, 0, -distance));
    }

    public void setDemoAnimation ()
    {
        if (gameLogic.isDemoCameraAnim)
            setCameraRotation (ERotating.right);
    }

    void runDemoAnimation ()
    {
        float newAngle;
        float animationTimer = timerToStartAnim - timeToRotate;

        newAngle = Mathf.Lerp (0f, -90f,  animationTimer / demoAnimationDuration );
        da = newAngle - angleForStartAnimation;
        transform.RotateAround (Vector3.zero, -transform.up, da);
        // Rotate light
        lightSources.transform.RotateAround (Vector3.zero, -transform.up, da); 
        angleForStartAnimation = newAngle;
    }

    public void startDemoAnimation ()
    {
        isDemoCameraAnimStarted = true;
    }
}
