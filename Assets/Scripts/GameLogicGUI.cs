using UnityEngine;
using UnityEngine.UI;

public static partial class StaticLogic
{
	public static Text scoreLabel;
    public static GameObject endUIGroup;
    public static GameObject inGameUIGroup;
    public static GameObject startUIGroup;

	static void InitGUILinks ()
	{
		scoreLabel = GameObject.Find ("Score").GetComponent<Text> ();
        endUIGroup = GameObject.Find("EndUIGroup");
        inGameUIGroup = GameObject.Find("InGameUIGroup");
        startUIGroup = GameObject.Find("StartUIGroup");
    }

	public static void AddScore (float scr)
	{
		player.score += scr;
		ChangeScore ();
	}

	static void ChangeScore ()
	{
		scoreLabel.text = "Score: " + player.score;
	}
}
