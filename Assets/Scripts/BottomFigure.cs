﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomFigure : MonoBehaviour
{
    public GameLogic gameLogic;

    public Transform cubeEdges; // cube edges prefab cube for figure

    public float thickness = 0.1f;

    public float indent = 0;

    public ERotating rotating;
    public float rotationSpeed = 5.0f;
    float angle;
    Quaternion originalRotationValue;

    Vector3 ancor; // the point around which the figure will be rotated

    public Dictionary<EFigureControlInterface,KeyCode> controlBinding;

    // Start is called before the first frame update
    void Start()
    {
        Transform temp;

        for (int i = 0; i < 4; i++)
        {
            temp = Instantiate(cubeEdges, Vector3.zero, Quaternion.identity ) as Transform;
            temp.parent = this.gameObject.transform;
            temp.GetComponent<CubeEdges>().setWidth ( thickness, indent );
        }

        angle = 0;
        originalRotationValue = transform.rotation; 

        controlBinding = StaticLogic.controlBinding;
    }

    // Update is called once per frame
    void Update()
    {
        if (rotating != ERotating.no)
        {
            StaticLogic.RotateObject (ref rotating, ref angle, rotationSpeed, transform.position, transform);
        }
        else
        {
            angle = 0;
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_down ] ) )
                rotating = ERotating.down;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_up ] ) )
                rotating = ERotating.up;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_right ] ) )
                rotating = ERotating.right;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_left ] ) )
                rotating = ERotating.left;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_counterclockwise ] ) )
                rotating = ERotating.counterclockwise;
            else 
            if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_clockwise ] ) )
                rotating = ERotating.clockwise;
        }
    }

    public void Create (Transform original)
    {
        int i = 0;

        transform.rotation = originalRotationValue;

        for (i = 0; i < 4; i++)
            transform.GetChild (i).gameObject.SetActive (false);


        for (i = 0; i < original.childCount; i++)
            if (original.GetChild (i).gameObject.activeSelf)
            {
                transform.GetChild (i).gameObject.SetActive (true);
                transform.GetChild (i).localPosition = original.GetChild (i).localPosition;

                transform.GetChild (i).GetComponent<CubeEdges>().setColor (original.GetChild (i).GetComponent<CubeAttributes>().color);
            }
        // Debug.Log ( original.GetChild (0).GetComponent<CubeAttributes>().color  + " " + original.GetChild (1).GetComponent<CubeAttributes>().color) ;

        ancor = transform.GetChild (0).position;
    }

    public void Draw (Vector3 pos, int childHitIndex)
    {
        transform.GetChild (0).gameObject.SetActive (true);
        transform.GetChild (1).gameObject.SetActive (true);

        transform.position = pos;
        if ( childHitIndex > 0 )
        {
            transform.Translate ( transform.GetChild (0).position - transform.GetChild (1).position, Space.World );
            //Debug.Log ( "Shift: " + (transform.GetChild (0).position - transform.GetChild (1).position) );
        }
    }

    public void HideFigure ()
    {
        foreach (Transform child in transform)
            child.gameObject.SetActive (false);
    }
}
