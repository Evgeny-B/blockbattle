﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour 
{
	public CubeBaseFigure baseFigure;

	public Figure figure;

    public FigureDublicate figureDublicate; // link to figure in front of small camera

	public Transform smallCamera;

    public dreamloLeaderBoard leaderBoard;

	public TextMeshProUGUI levelTillFall;

    public LevelSettings levelSettings;

    public LevelGenerator levelGenerator;

	public bool isScoreOn = false;

    public bool isDemo = false; // after one round switch to Levels scene
    public float demoTime = 1.5f; // sec till screen switch
    float demoTimer = 0;

    public bool isDemoCameraAnim = false; // Show base figure in motion
    public float timeToRotate = 1.0f;

    public bool isTutorialOn = false;

    public bool isOpponent = false; // emulationg opponent

    public bool isCycleColors = false; // Use CubeColorsArraysCycle for coloring cubes

    public bool isOneColor = false; // Use only one color from each

    public static bool isPause = false;
    public GameObject pauseMenuPanel;

	void Awake ()
	{
        if (levelSettings.rewriteSettings)
        {
            LevelSettingsStatic.firstGold = levelSettings.firstGold;
            LevelSettingsStatic.gameDifficulty = levelSettings.gameDifficulty;
            LevelSettingsStatic.levelName = levelSettings.levelName;
            LevelSettingsStatic.nothingFall = levelSettings.nothingFall;
            LevelSettingsStatic.calibrate = levelSettings.calibrate;
        }

        levelGenerator.Init (LevelSettingsStatic.levelName);

		StaticLogic.Init ();
	}

    void Start ()
    {
        foreach ( GameObject go in GameObject.FindGameObjectsWithTag("opponent") )
        {
            go.SetActive ( isOpponent );
        }

        if (!isTutorialOn)
            baseFigure.Init ();

        if (!isDemoCameraAnim)
            StaticLogic.SwitchObjectsLayers (); 

        RectTransform objectRectTransform = GameObject.Find("Canvas").GetComponent<RectTransform> ();
        Debug.Log("Canvas width: " + objectRectTransform.rect.width + ", height: " + objectRectTransform.rect.height);
    }
 
    // Update is called once per frame
    void Update () 
    {
        if ( ( StaticLogic.gameState == StaticLogic.EGameState.start ) || ( StaticLogic.gameState == StaticLogic.EGameState.ingame ) )
        {
            if ( Input.GetKeyDown ( KeyCode.Escape ) )
            {
                if (isPause)
                    ResumeGame ();
                else
                    PauseGame ();
            }
        }

        if ( (isDemoCameraAnim) && ( StaticLogic.gameState == StaticLogic.EGameState.start ) && (Camera.main.GetComponent<CameraControl>().isDemoCameraAnimFinished) )
            StaticLogic.SwitchObjectsLayers (); 

        if (levelTillFall != null)
            levelTillFall.SetText ( figure.levelTillFall.ToString () );

        // Condition to end game
        if ( ( StaticLogic.gameState == StaticLogic.EGameState.ingame ) && ( !isTutorialOn ) )
        {
            if ( baseFigure.totalGoldCubes == 0 )
            {
                //TODO: Player can create and destroy new cubes
                if ( baseFigure.destroyedRegularCubes == baseFigure.totalRegularCubes )
                    StaticLogic.SwitchObjectsLayers ();
            }
            else
            {
                if (LevelSettingsStatic.firstGold)
                {
                    if ( baseFigure.destroyedGoldCubes > 0 )
                        StaticLogic.SwitchObjectsLayers ();
                }
                else
                {
                    if ( baseFigure.destroyedGoldCubes == baseFigure.totalGoldCubes )
                        StaticLogic.SwitchObjectsLayers ();
                }
            }
        }

        //Debug.Log ("gameState" + StaticLogic.gameState.ToString());
        if ( (StaticLogic.gameState == StaticLogic.EGameState.end) && (isDemo) )
        {
            if ( demoTimer >= demoTime )
                SceneManager.LoadScene("levels_menu");
            else
                demoTimer += Time.deltaTime;
        }
    }

    public void ResumeGame ()
    {
        pauseMenuPanel.SetActive (false);
        Time.timeScale = 1f;
        isPause = false;
    }

    void PauseGame ()
    {
        pauseMenuPanel.SetActive (true);
        Time.timeScale = 0f;
        isPause = true;
    }
}

public enum ERotating {no = 0, right = 1, left = -1, up = 2, down = -2, clockwise = 3, counterclockwise = -3};

public enum EDirection {right, left, up, down, forward, backward};

public enum EGameDifficulty {easy = 0, normal = 1, hard = 2};

public enum EFigureControlInterface {turn_right, turn_left, turn_up, turn_down, turn_clockwise, turn_counterclockwise, move_left, move_right, move_up, move_down, fall, next_color, prev_color};

public static partial class StaticLogic
{
    public static GameLogic gameLogic; // Static link to Game Logic

	public static CubeBaseFigure baseFigure; // Static link to base figure

	public static Figure figure; // Static link to figure
    public static FigureDublicate figureDublicate; // Static link to figure in front of small camera

	public enum EGameState {start = 0, ingame = 1, end = 2, idle = 3};

    public static EGameState gameState;

    public static bool isTurnOffTutorial = false; // for loading w/o tutorial

	public static CPlayer player;    

    public static Dictionary<EFigureControlInterface,KeyCode> controlBinding;

    public static Dictionary<CubeColors,string[]> cubeColorsArrays;
    
    // Use this for not game init
    public static void BareInit () 
    {
        cubeColorsArrays = new Dictionary<CubeColors,string[]> ();

        if ( ( gameLogic != null ) && (gameLogic.isCycleColors) )
        {
            cubeColorsArrays.Add ( CubeColors.red, CubeColorsArraysCycle.redColors );
            cubeColorsArrays.Add ( CubeColors.white, CubeColorsArraysCycle.whiteColors );
            cubeColorsArrays.Add ( CubeColors.yellow, CubeColorsArraysCycle.yellowColors );
        }
        else
        {
            cubeColorsArrays.Add ( CubeColors.red, CubeColorsArrays.redColors );
            cubeColorsArrays.Add ( CubeColors.white, CubeColorsArrays.whiteColors );
            cubeColorsArrays.Add ( CubeColors.yellow, CubeColorsArrays.yellowColors );
            cubeColorsArrays.Add ( CubeColors.blue, CubeColorsArrays.blueColors );
        }
    }

	// Use this for initialization
	public static void Init () 
	{
        gameLogic = GameObject.Find("GameLogic").GetComponent<GameLogic>();

        if (isTurnOffTutorial)
        {
            gameLogic.isTutorialOn = false;
            GameObject.Find("Tutorial").gameObject.SetActive (false);
        }

        baseFigure = gameLogic.baseFigure;
        figureDublicate = gameLogic.figureDublicate;
        figure = gameLogic.figure;

		InitGUILinks ();
        
		player = new CPlayer ();

        StaticLogic.InitControlBinding ();

        StaticLogic.BareInit ();

        StaticLogic.Reset ();
	}

    public static Dictionary<EFigureControlInterface,KeyCode> InitControlBinding ()
    {
        controlBinding = new Dictionary<EFigureControlInterface,KeyCode> ();
        controlBinding.Add ( EFigureControlInterface.turn_right, KeyCode.D );
        controlBinding.Add ( EFigureControlInterface.turn_left, KeyCode.A );
        controlBinding.Add ( EFigureControlInterface.turn_up, KeyCode.W );
        controlBinding.Add ( EFigureControlInterface.turn_down, KeyCode.S );
        controlBinding.Add ( EFigureControlInterface.turn_clockwise, KeyCode.Q );
        controlBinding.Add ( EFigureControlInterface.turn_counterclockwise, KeyCode.E );
        controlBinding.Add ( EFigureControlInterface.move_left, KeyCode.LeftArrow );
        controlBinding.Add ( EFigureControlInterface.move_right, KeyCode.RightArrow );
        controlBinding.Add ( EFigureControlInterface.move_up, KeyCode.UpArrow );
        controlBinding.Add ( EFigureControlInterface.move_down, KeyCode.DownArrow );
        controlBinding.Add ( EFigureControlInterface.fall, KeyCode.Space );
        controlBinding.Add ( EFigureControlInterface.next_color, KeyCode.F );

        return controlBinding;
    }

    public static void Reset ()
    {
        Time.timeScale = 1f;

        gameState = EGameState.start;

        GameLogic.isPause = false;

        Debug.Log ("startUIGroup " + startUIGroup.ToString() );
        Debug.Log ("levelTitle " + startUIGroup.transform.Find ("LevelTitle").ToString() );
        startUIGroup.transform.Find ("LevelTitle").GetComponent<TextMeshProUGUI> ().SetText ( LevelSettingsStatic.levelTitle );

        startUIGroup.SetActive(true);
        inGameUIGroup.SetActive(false);
        endUIGroup.SetActive(false);

        if (!gameLogic.isScoreOn)
            scoreLabel.gameObject.SetActive (false);

        Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("Start"));
        Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("InGame"));
        Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("MiniFigure"));        
        Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("End"));       
    } 

	public static void SwitchObjectsLayers ()
	{
		gameState ++;

		if (gameState == EGameState.ingame)
		{
            startUIGroup.SetActive(false);
            inGameUIGroup.SetActive(true);

			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("Start"));
			Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("InGame"));
			Camera.main.cullingMask |=  (1 << LayerMask.NameToLayer("MiniFigure"));
			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("End"));
		}
		else
		if (gameState == EGameState.end)
		{
            baseFigure.gameObject.SetActive(false);
            inGameUIGroup.SetActive(false);
            endUIGroup.SetActive(true);
            
			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("Start"));
			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("InGame"));
			Camera.main.cullingMask &=  ~(1 << LayerMask.NameToLayer("MiniFigure"));
			Camera.main.cullingMask |= (1 << LayerMask.NameToLayer("End"));
		}
		
        Debug.Log ("SwitchObjectsLayers gameState " + gameState.ToString() );
	}

	public static bool RotateObject (ref ERotating rotating, ref float angle, float rotationSpeed, Vector3 zeroPoint, Transform transform)
	{
		float da;
		bool res = false;

		switch (rotating)
		{
		case ERotating.right:
			da = 90.0f * Time.deltaTime * rotationSpeed;
			if (angle + da > 90.0f)
			{
				transform.RotateAround (zeroPoint, -Camera.main.transform.up, 90.0f - angle);
				rotating = ERotating.no;
				angle = 0.0f;
				res = true;
			}
			else
			{
				transform.RotateAround (zeroPoint, -Camera.main.transform.up, da);
				angle += da;
			}
			break;
		case ERotating.left:
			da = -90.0f * Time.deltaTime * rotationSpeed;
			if (angle + da < -90.0f)
			{
				transform.RotateAround (zeroPoint, -Camera.main.transform.up, -90.0f - angle);
				rotating = ERotating.no;
				angle = 0;
				res = true;
			}
			else
			{
				transform.RotateAround (zeroPoint, -Camera.main.transform.up, da);
				angle += da;
			}
			break;
		case ERotating.up:
			da = 90.0f * Time.deltaTime * rotationSpeed;
			if (angle + da > 90)
			{
				transform.RotateAround (zeroPoint, Camera.main.transform.right, 90.0f - angle);
				rotating = ERotating.no;
				angle = 0;
				res = true;
			}
			else
			{
				transform.RotateAround (zeroPoint, Camera.main.transform.right, da);
				angle += da;
			}
			break;
		case ERotating.down:
			da = -90.0f * Time.deltaTime * rotationSpeed;
			if (angle + da < -90.0f)
			{
				transform.RotateAround (zeroPoint, Camera.main.transform.right, -90.0f - angle);
				rotating = ERotating.no;
				angle = 0;
				res = true;
			}
			else
			{
				transform.RotateAround (zeroPoint, Camera.main.transform.right, da);
				angle += da;
			}
			break;
        case ERotating.clockwise:
            da = 90.0f * Time.deltaTime * rotationSpeed;
            if (angle + da > 90.0f)
            {
                transform.RotateAround (zeroPoint, Camera.main.transform.forward, 90.0f - angle);
                rotating = ERotating.no;
                angle = 0;
                res = true;
            }
            else
            {
                transform.RotateAround (zeroPoint, Camera.main.transform.forward, da);
                angle += da;
            }
            break;
        case ERotating.counterclockwise:
            da = -90.0f * Time.deltaTime * rotationSpeed;
            if (angle + da < -90.0f)
            {
                transform.RotateAround (zeroPoint, Camera.main.transform.forward, -90.0f - angle);
                rotating = ERotating.no;
                angle = 0;
                res = true;
            }
            else
            {
                transform.RotateAround (zeroPoint, Camera.main.transform.forward, da);
                angle += da;
            }
            break;
		}

		return res;
	}

	public static EDirection TurnDirection (Vector3 pos)
	{
		EDirection relDir = EDirection.right;
		if (Vector3.Angle (Vector3.right, pos) < 10)
			relDir = EDirection.right;
		else
			if (Vector3.Angle (-Vector3.right, pos) < 10)
				relDir = EDirection.left;
		else
			if (Vector3.Angle (Vector3.up, pos) < 10)
				relDir = EDirection.up;
		else
			if (Vector3.Angle (-Vector3.up, pos) < 10)
				relDir = EDirection.down;
		else
			if (Vector3.Angle (Vector3.forward, pos) < 10)
				relDir = EDirection.forward;
		else
			if (Vector3.Angle (-Vector3.forward, pos) < 10)
				relDir = EDirection.backward;
		//		Debug.Log (StaticLogic.figure.direction.ToString ());
		return relDir;
	}

	public static Vector3 GetUnitVectorFromDirection (EDirection dir)
	{
		Vector3 ans = Vector3.right;
		switch (dir) 
		{
		case EDirection.right:
			ans = Vector3.right;
			break;
		case EDirection.left:
			ans = -Vector3.right;
			break;
		case EDirection.up:
			ans = Vector3.up;
			break;
		case EDirection.down:
			ans = -Vector3.up;
			break;
		case EDirection.forward:
			ans = Vector3.forward;
			break;
		case EDirection.backward:
			ans = -Vector3.forward;
			break;
		}
		return ans;
	}
}
